$(document).ready(function () {

$(window).on("scroll", function() {
    if($(window).scrollTop() > 300) {
        $(".header").addClass("wrapper");
    } else {
        $(".header").removeClass("wrapper");
    }
});



$window = $(window);
$slick_slider = $('.slider');
settings = {
    slide: '.slide',
    arrows: true,
    prevArrow: '.slick-prev',
    nextArrow: '.slick-next',
    dots: true,
    mobileFirst: true,
    autoplay: true,
    autoplaySpeed: 3000
};
$slick_slider.slick(settings);

$window.on('resize', function() {
  if ($window.width() < 600) {
    if ($slick_slider.hasClass('slick-initialized'))
      $slick_slider.slick('unslick');
    return
  }
  if ( ! $slick_slider.hasClass('slick-initialized'))
    return $slick_slider.slick(settings);
});

// вариант
// window.addEventListener("resize", function() {
//   if (window.innerWidth <= 768) {
//     $('.your-slider').slick('unslick');
//     sliderIsLive = false;
//   }
//   else {
//     if (sliderIsLive) {
//       $('.your-slider').slick();
//       sliderIsLive = true;
//     }
//   }
// });

// не работает без перезагрузки страницы при изменении окна
    // $('.slider').slick({
    //     slide: '.slide',
    //     // autoplay: true,
    //     // autoplaySpeed: 1000,
    //     // infinite: true,
    //     arrows: true,
    //     // appendArrows: '.arrow',
    //     prevArrow: '.slick-prev',
    //     nextArrow: '.slick-next',
    //     dots: true,
    //     mobileFirst: true,
    //     responsive: [
    //         {
    //             breakpoint: 768,
    //             settings: 'slick'
    //         },
    //         {
    //             breakpoint: 320,
    //             settings: "unslick"
    //         }
    //     ]
    //     // fade: true,
    //     // easing: 'swing',
    //     // speed: 800,
    // });

 $('.post-slider').slick({
        slide: '.slides',
        // autoplay: true,
        // autoplaySpeed: 3000,
        arrows: true,
        dots: false,
        infinite: true,
        // slidesToShow: 3,
        // slidesToScroll: 1,
        prevArrow: '.slick-preve',
        nextArrow: '.slick-nexte',
        // centerMode: 'true',
        // initialSlide: '0',
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });   

$('a.animated').hover(
     function() {
        $(this).addClass('rubberBand');
     },
     function() {
        $(this).removeClass('rubberBand');
     }
);

$('div.input-container > input, div.input-container > textarea')
        .on('focus', function (e) {
            $(this).prev('label').hide();
        })
        .on('blur', function (e) {
            if ($(this).val().trim() === '')
                 $(this).prev('label').show();
        });

//мой первый вариант (один общий div, в нем несколько input-ов с label-ами)
    // document.getElementById("cont").addEventListener("focusin", function(e) {
    // e.target.nextSibling.style.visibility = 'hidden';
    // }, false);
    // document.getElementById("cont").addEventListener("focusout", function(e) {
    // if(e.target.value.trim() === '') {
    // e.target.nextSibling.style.visibility = 'visible';
    // }
    // }, false);

});




function OpenWin(url) {
    window.open(url, "window_name", "width=300, height=300");
};


var map,
    marker,
    center = {
        lat: -7.925948,
        lng: 112.629378
    },
    image = 'img/pin.png';

function initMap() {
    var mapDiv = document.getElementById('map');
    map = new google.maps.Map(mapDiv, {
    center: center,
    zoom: 15,
    styles: [
        {
            "elementType": "geometry",
            "stylers": [{
                "color": "#f5f5f5"
            }]
        },
        {
            "elementType": "labels.icon",
            "stylers": [{
                "visibility": "off"
            }]
        },
        {
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#616161"
            }]
        },
        {
        "elementType": "labels.text.stroke",
        "stylers": [
        {
        "color": "#f5f5f5"
        }
        ]
        },
        {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#bdbdbd"
        }
        ]
        },
        {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#eeeeee"
        }
        ]
        },
        {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#757575"
        }
        ]
        },
        {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#e5e5e5"
        }
        ]
        },
        {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#9e9e9e"
        }
        ]
        },
        {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#ffffff"
        }
        ]
        },
        {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#757575"
        }
        ]
        },
        {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#dadada"
        }
        ]
        },
        {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#616161"
        }
        ]
        },
        {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#9e9e9e"
        }
        ]
        },
        {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#e5e5e5"
        }
        ]
        },
        {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#eeeeee"
        }
        ]
        },
        {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
        {
        "color": "#c9c9c9"
        }
        ]
        },
        {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
        {
        "color": "#9e9e9e"
        }
        ]
        }
        ]
    });

    var marker = new google.maps.Marker({
        position: {
            lat : -7.923398,
            lng: 112.636201
        },
        map: map,
        icon: image
    });
};